# Hand grasping

<!--
[![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/bci-gest/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/bci-gest/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/bci-gest/)
-->
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)  

## Foreword

This project is a fork of : [Léa pillette's protocol](https://gitlab.inria.fr/lpillett/bci-gest) associated with [the article](Documentation/Multi-Session%20Influence%20of%20Two%20Modalities%20of%20Feedback%20and%20Their%20Order%20of%20Presentation%20on%20MI-BCI%20User%20Training.pdf) present on this repository . This new version aims to be used in a recent post-stroke rehabilitation protocol in a simplified way with many automated elements.

## Description

This game is based on the visualization of Hand grasping with communication with Arduino gloves which have motors allowing vibrotactile feedback.  
Communication with the gloves is via a COM port.  
The game requires the result of a classifier which is received by LSL from [OpenViBE](http://openvibe.inria.fr/).  
In parallel, we create an OpenViBE protocol (group of senario).  
We can use it to gamify Mental Task Brain Computer Interface during research protocol.

## Dependencies

- [LSL4Unity](https://gitlab.inria.fr/openvibe/unity-games/LSL4Unity)

## Install

- Pulling this repository : `git pull https://gitlab.inria.fr/openvibe/unity-games/bci-gest.git` or with any GUI for git, like GitKraken.
- Pulling the submodules : `git submodule update --init --recursive` or with any GUI for git, like GitKraken.

To use with gloves (with an Arduino board and 2 groups of 2 motors, for other configurations, modify the  file [`Arduino/Vibrotactile_Gloves/Vibrotactile_Gloves.ino`](Arduino/Vibrotactile_Gloves/Vibrotactile_Gloves.ino) and potentially the file [`Assets/Scripts/Arduino.cs`](Assets/Scripts/Arduino.cs)) :

- Install the Arduino IDE. To update your card and easily check the COM port used by your card. This port can sometimes change by disconnecting and reconnecting your card, the verification must be recurrent.
- For any other system, no warranty or maintenance can be offered.

## How to use (our protocol)

More Détails with more informations and screenshot in [this file](Documentation/Protocol.md)  
For our protocol, here are the different steps to follow:

- The first time:
  - **Game** Create Subject standard files and folder using Games.
  - **Game** Check that the COM port is correctly Identify by the game (change it if necessary)
  - **Game** Set the movement speed for the subject.
  - **Game** Set the vibration intensity for the subject.
- Then and each time:
  - **OV** Monitoring, during cap installation.
  - **OV** Baseline, without the basic visual of the game.
  - **OV/Game** Baseline, with the visual of the game (in static without movement or vibrations)
  - **OV/Game** Acquisition, A Sham Feedback is offered to the subject on 2 acquisition runs.
  - **OV** Concatenate Files, To combine the two acquisition files.
  - **OV** Train Classifier, with the new concatenate file, train the classifier.
  - **OV** Get Distances, for any input file, it outputs the classifier output distances in a `Distances.csv` file.
  - **Game** Get Normalization, allows with a `Distances.csv` file to update the settings allowing a normalization of the visualization.
  - **OV/Game** Online Run.

## Screenshots

| ![Menu](Documentation/Screenshot/Menu.png) | ![Game](Documentation/Screenshot/Game.png) |
|:------------------------------------------:|:------------------------------------------:|
|                    Menu                    |                    Game                    |
