positive_only (Y for yes or N for no): Y
-
positive_bias (Y for yes or N for no): N
strength-bias (in [0.0;0.5], with 1 decimal): 0.0
-
MIN_intensity_tactile-fb_left: 50
MAX_intensity_tactile-fb_left: 200
MIN_intensity_tactile-fb_right: 50
MAX_intensity_tactile-fb_right: 200
-
nb_thresholds_tactile-fb_left: 5.0
nb_thresholds_tactile-fb_right: 5.0
-
MIN_intensity_visual-fb_left: 0.05
MAX_intensity_visual-fb_left: 1
MIN_intensity_visual-fb_right: 0.05
MAX_intensity_visual-fb_right: 1
-
nb_thresholds_visual-fb_left: 5.0
nb_thresholds_visual-fb_right: 5.0
-
Median_left: 0.05209176617867671
MAD_left: 0.11164122412505956
Median_right: 0.04822608521630203
MAD_right: 0.1097139167579704
-