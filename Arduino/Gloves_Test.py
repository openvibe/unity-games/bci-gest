import serial
import time
from datetime import datetime

serialPort     = serial.Serial('COM9', 9600, timeout=1)
R_Motors       = range(12)
R_Motors_Group = range(2)
R_intensity    = range(50,201,25)

# Running Gloves Msg
print(serialPort.readline())

# Run and Stop Motor alternatively
for x in R_Motors:
	current_time = datetime.now().strftime("%H:%M:%S")
	print(str(current_time)+" : Run Motor " + str(x))
	serialPort.write(("m"+str(x)+"\r\n").encode())
	print(serialPort.readline())
	time.sleep(1)
	serialPort.write("a\r\n".encode())
	print(serialPort.readline())

# Run and Stop Group alternatively
for x in R_Motors_Group:
	current_time = datetime.now().strftime("%H:%M:%S")
	print(str(current_time)+" : Run Motor Group " + str(x))
	serialPort.write(("g"+str(x)+"\r\n").encode())
	print(serialPort.readline())
	time.sleep(1)
	serialPort.write("a\r\n".encode())
	print(serialPort.readline())

# Run and Change intensity
for x in R_intensity:
	current_time = datetime.now().strftime("%H:%M:%S")
	print(str(current_time)+" : Run Motor Group 0 with Intensity " + str(x))
	serialPort.write(("g0i"+str(x)+"\r\n").encode())
	print(serialPort.readline())
	print(serialPort.readline())
	time.sleep(1)
	serialPort.write("a\r\n".encode())
	print(serialPort.readline())
