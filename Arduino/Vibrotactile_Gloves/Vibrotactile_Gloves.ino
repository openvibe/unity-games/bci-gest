
#define MAX_MOTOR 12
#define MOTOR_BY_SIDE 2
#define MOTOR_GROUP 2
#define VERBOSE 1

//-------------------------------------------------------
uint8_t pinMot[MAX_MOTOR] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
uint8_t pinGroup[MOTOR_GROUP][MOTOR_BY_SIDE] = {{0, 1}, {4, 5}}; // ID on pinMot array 0 for left 1 for right
int intensity = 50;

//-------------------------------------------------------
void setup()
{
  for (uint8_t i = 0; i < MAX_MOTOR; i++) {
    pinMode(pinMot[i], OUTPUT);
    analogWrite(pinMot[i], 0);
  }
  Serial.begin(9600);
  Serial.println("***** Running Gloves *****");
}

//-------------------------------------------------------
void loop()
{
  while (Serial.available()) {
    String msg;
    char c = Serial.read();
    switch (c) {
    case 'm':   // Motor Activation
      msg = Serial.readStringUntil('\n');
      if (msg.length()) { 
        int pin = msg.toInt(); 
        analogWrite(pinMot[pin], intensity);
        if (VERBOSE) { Serial.println("Motor " + String(pin) + " activation"); }
      }
      break;

    case 'g':   // Groupe Activation
      msg = Serial.readStringUntil('\n');
      if (msg.length()) {
        int group = String(msg[0]).toInt();
        msg.remove(0,1);
        if (group == 0 || group == 1) {
          if (msg.length() > 2) {
            if (msg[0] == 'i') {
              msg.remove(0,1);
              intensity = constrain(msg.toInt(), 0, 255);
              if (VERBOSE) { Serial.println("Intensity changed to " + String(intensity)); }
            }
          }
          for (uint8_t i = 0; i < MOTOR_BY_SIDE; i++) { analogWrite(pinMot[pinGroup[group][i]], intensity); }
          if (VERBOSE) { Serial.println("Motor group " + String(group) + " activation"); }
        }
        else if (VERBOSE) { Serial.println("Error : invalid group motor"); }
      }

      break;

    case 'i':   // Intensity Change
      msg = Serial.readStringUntil('\n');
      if (msg.length()) {
        intensity = constrain(msg.toInt(), 0, 255);
        if (VERBOSE) { Serial.println("Intensity changed to " + String(intensity)); }
      }
      else if (VERBOSE) { Serial.println("Error : uknown intensity command"); }
      break;

    case 'a':   // Stop all
      Serial.readStringUntil('\n');
      for (uint8_t i = 0; i < MAX_MOTOR; i++) { analogWrite(pinMot[i], 0); }
      if (VERBOSE) { Serial.println("All motors stopped"); }
      break;

    default:
      while (Serial.available()) { Serial.read(); }
      if (VERBOSE) {
        Serial.println("Error : uknown command. Allowed command are :");
        Serial.println("  mX : Run motor X where X define a integer");
        Serial.println("  gX : Run motor group X where X define a integer");
        Serial.println("  gXiY : Run motor group X with Y intensity where X define a integer and Y too");
        Serial.println("  iX : Change Intensity by X where X define a integer");
        Serial.println("  a : Stop all motors");
      }
      break;
    }
  }
}
