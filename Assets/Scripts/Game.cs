﻿using System.Collections.Generic;
using UnityEngine;

/// <summary> Possible trials during game. </summary>
public enum TrialState { Rest, Left, Right, Ball, Fork, Circle, Nut }

/// <summary> Main Controller for the game. </summary>
public class Game : MonoBehaviour
{
	#region Members

	// Constant
	private static readonly string[] CanvasList = { "Cross", "Arrow Left", "Arrow Right", "Text", "Black Screen", "Instruction", "Information" };

	// Private
	private static GameObject                     _human  = null;
	private static Dictionary<string, GameObject> _canvas = new Dictionary<string, GameObject>();

	private Mode _mode = null;

	// Public
	// LSL
	public static FloatInlet       SignalInlet = null;
	public static StimulationInlet StimInlet   = null;

	public static double LastStimTime   = 0.0;
	public static double LastSignalTime = 0.0;

	// Animation
	public static string    AnimTrigger = "Rest";
	public static Animator  Anim        = null;
	public static Wristband WristbandLeft, WristbandRight;

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	/// <summary> Is called once when the program starts </summary>
	private void Awake()
	{
		GetObjects();
		DeactivateUI();
		switch (Settings.Mode) {
			case GameMode.FindIntensity:
				_mode = new FindIntensity();
				break;
			case GameMode.Baseline:
				_mode = new Baseline();
				break;
			case GameMode.Online:
				_mode = new Online();
				break;
			case GameMode.AnimationTest:
				_mode = new AnimationTest();
				break;
			default: throw new System.ArgumentOutOfRangeException();
		}
		_mode.Init();
		SignalInlet.ResolveStream();
		StimInlet.ResolveStream();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Is called once per frame </summary>
	private void Update()
	{
		//Used to have the scripts running even when the focus is not on the Unity window
		Application.runInBackground = true;
		if (!SignalInlet.IsSolved()) { SignalInlet.ResolveStream(); } // Try to resolve an other time
		if (!StimInlet.IsSolved()) { StimInlet.ResolveStream(); }     // Try to resolve an other time
		_mode.Update();
		Anim.SetTrigger(AnimTrigger); // Must be run each time ?
		//var str = Arduino.ReadLine();
		//if (str != "") { Debug.Log(str); }
		if (Input.GetKeyDown(KeyCode.Space)) { Arduino.StopMotors(); } // In case the gloves get out of control
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Is called once when the application is closing. </summary>
	private void OnApplicationQuit() { ResetObjects(); }

	#endregion

	#region Object Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the usefull objects in scene. </summary>
	private void GetObjects()
	{
		ResetObjects();
		Arduino.Open();
		_human = transform.Find("Small Human").gameObject;
		Anim   = _human.GetComponent<Animator>();
		var canvas = transform.Find("Canvas");
		foreach (var s in CanvasList) { _canvas.Add(s, canvas.Find(s).gameObject); }
		SignalInlet    = gameObject.GetComponent<FloatInlet>();
		StimInlet      = gameObject.GetComponent<StimulationInlet>();
		WristbandLeft  = _human.transform.Find("Wristband Left").gameObject.GetComponent<Wristband>();
		WristbandRight = _human.transform.Find("Wristband Right").gameObject.GetComponent<Wristband>();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Remove the link of objects in scene and close Arduino stream. </summary>
	private static void ResetObjects()
	{
		Arduino.Close();
		_human = null;
		Anim   = null;
		_canvas.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the animation name (we used trigger on animation controller). </summary>
	/// <param name="state"> The state. </param>
	/// <returns> The name of the trigger. </returns>
	/// <exception cref="System.ArgumentOutOfRangeException">state - null</exception>
	public static string GetAnim(TrialState state)
	{
		switch (state) {
			case TrialState.Rest:   return "Rest";
			case TrialState.Left:   return "Grasping Left";
			case TrialState.Right:  return "Grasping Right";
			case TrialState.Ball:   return "Ball";
			case TrialState.Fork:   return "Fork";
			case TrialState.Circle: return "Circle";
			case TrialState.Nut:    return "Nut";
			default:                throw new System.ArgumentOutOfRangeException(nameof(state), state, null);
		}
	}

	//----------------------------------------------------------------------------------------------------
	public static void SetAnim(string anim) { AnimTrigger = anim; }

	//----------------------------------------------------------------------------------------------------
	public static void SetSpeed(float speed) { Anim.speed = speed; }

	#endregion

	#region UI Management

	//----------------------------------------------------------------------------------------------------
	public static void SetActiveUI(bool cross       = false, bool arrowLeft   = false, bool arrowRight = false, bool text = false, bool blackScreen = false,
								   bool instruction = false, bool information = false)
	{
		_canvas["Cross"].SetActive(cross);
		_canvas["Arrow Left"].SetActive(arrowLeft);
		_canvas["Arrow Right"].SetActive(arrowRight);
		_canvas["Text"].SetActive(text);
		_canvas["Black Screen"].SetActive(blackScreen);
		_canvas["Instruction"].SetActive(instruction);
		_canvas["Information"].SetActive(information);
	}

	//----------------------------------------------------------------------------------------------------
	public static void DeactivateUI() { SetActiveUI(); }

	//----------------------------------------------------------------------------------------------------
	public static void SetTextUI(string text) { _canvas["Text"].GetComponent<UnityEngine.UI.Text>().text = text; }

	//----------------------------------------------------------------------------------------------------
	public static void SetInstructiontUI(string text) { _canvas["Instruction"].GetComponent<UnityEngine.UI.Text>().text = text; }

	//----------------------------------------------------------------------------------------------------
	public static void SetInformationUI(string text) { _canvas["Information"].GetComponent<UnityEngine.UI.Text>().text = text; }

	#endregion
}
