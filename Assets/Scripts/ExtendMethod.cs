﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary> Add Functions to objects in parameters. </summary>
static class ExtendMethod
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the median of <c>IEnumerable</c>. </summary>
	/// <param name="source"> The <c>IEnumerable</c>. </param>
	/// <returns> Median. </returns>
	/// <exception cref="System.InvalidOperationException">Empty collection</exception>
	public static float GetMedian(this IEnumerable<float> source)
	{
		// Create a copy of the input, and sort the copy
		var temp = source.ToArray();
		Array.Sort(temp);
		var count = temp.Length;
		if (count == 0) { throw new InvalidOperationException("Empty collection"); }
		if (count % 2 == 0) {
			// count is even, average two middle elements
			var a = temp[count / 2 - 1];
			var b = temp[count / 2];
			return (a + b) / 2;
		}
		// count is odd, return the middle element
		return temp[count / 2];
	}
}
