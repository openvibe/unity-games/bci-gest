﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary> Main Controller for the Start Menu. </summary>
public class StartMenu : MonoBehaviour
{
	#region Members

	private static readonly string[]                      CheckedList = { "Subject", "Intensity", "Baseline" };
	private static          Dictionary<string, Transform> _checked    = new Dictionary<string, Transform>();
	private static          TMPro.TMP_InputField          _comPortInput;
	private static          TMPro.TMP_Text                _errorText;

	private static readonly Color ErrorColor = new Color(0.8F, 0.0F, 0.0F, 1.0F);
	private static readonly Color ValidColor = new Color(0.0F, 0.5F, 0.0F, 1.0F);

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	/// <summary> Is called once when the program starts </summary>
	private void Awake()
	{
		GetObjects();
		Arduino.GetComPort();
		_comPortInput.text = Arduino.ComPort;
		UpdateChecked();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Is called once per frame </summary>
	private void Update()
	{
		if (!Input.GetKeyDown(KeyCode.Escape)) { return; }
		Debug.Log("Quit Application");
		Application.Quit();
	}

	#endregion

	#region Object Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the usefull objects in scene. </summary>
	private void GetObjects()
	{
		ResetObjects();
		var group = transform.Find("Main/Checked");
		foreach (var s in CheckedList) { _checked.Add(s, group.Find(s)); }
		_comPortInput = transform.Find("Main/Port COM Input").GetComponent<TMPro.TMP_InputField>();
		_errorText    = transform.Find("Main/Error Text").GetComponent<TMPro.TMP_Text>();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Remove the link of objects in scene. </summary>
	private static void ResetObjects() { _checked.Clear(); }

	#endregion

	#region Buttons Functions

	//----------------------------------------------------------------------------------------------------
	public void ChangeID(string s) { SettingsManager.Settings.ChangeID(s == "" ? "0" : s); }

	//----------------------------------------------------------------------------------------------------
	public void ChangePort(string s)
	{
		// Test input
		var ports = Arduino.GetComPortList();
		var found = Arduino.Exist(s);

		// Change Input
		Arduino.ChangePort(found ? s : "No Port");
		if (found) { return; }
		var str = "Invalid port selected, ";
		if (ports.Length != 0) {
			str += "available are : ";
			str =  ports.Aggregate(str, (current, port) => current + (port + " "));
		}
		else { str += "no port are available."; }
		UpdateText(str);
	}

	//----------------------------------------------------------------------------------------------------
	public void CheckSubject()
	{
		SettingsManager.Settings.CheckSubject();
		UpdateChecked();
		Debug.Log("CheckSubject");
	}

	//----------------------------------------------------------------------------------------------------
	public void FindIntensity()
	{
		if (!SettingsManager.Settings.CanCheckIntensity()) {
			UpdateText("Check Subject Before.");
			return;
		}
		Settings.Mode = GameMode.FindIntensity;
		Debug.Log("FindIntensity");
		UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
	}

	//----------------------------------------------------------------------------------------------------
	/*
	public void Baseline()
	{
		if (!SettingsManager.Settings.CanMakeBaseline()) {
			UpdateText("Check Subject Before (maybe intensity too).");
			return;
		}
		Settings.Mode = GameMode.Baseline;
		Debug.Log("Baseline");
		UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
	}
	*/

	//----------------------------------------------------------------------------------------------------
	public void Run()
	{
		if (!SettingsManager.Settings.CanMakeRun()) {
			UpdateText("Check Subject Before (maybe intensity too).");
			return;
		}
		Settings.Mode = GameMode.Online;
		Debug.Log("Run");
		UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
	}

	//----------------------------------------------------------------------------------------------------
	public void GetNormalization()
	{
		if (!SettingsManager.Settings.CanMakeNormalization()) {
			UpdateText("Check Subject Before and create Distances.csv file.");
			return;
		}
		Normalization.GetNormalization();
		UpdateText("Normalization Done", false);

		// Open file
		//var    path = SFB.StandaloneFileBrowser.OpenFilePanel("Open File", "", "", false);
		//if (path.Length != 0) { Debug.Log("It's my file : " + path); }
	}

	//----------------------------------------------------------------------------------------------------
	public void RetrievePerf()
	{
		if (!SettingsManager.Settings.CanMakeNormalization()) {
			UpdateText("Check Subject Before and create Distances.csv file.");
			return;
		}
		var res = RetrievePerformance.GetPerformance();
		UpdateText(res, false);
	}

	//----------------------------------------------------------------------------------------------------
	public void AnimationTest()
	{
		Settings.Mode = GameMode.AnimationTest;
		Debug.Log("Animation Test");
		UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
	}

	#endregion

	#region Utils

	//----------------------------------------------------------------------------------------------------
	private static void UpdateChecked()
	{
		UpdateChecked(_checked["Subject"],   SettingsManager.Settings.Checked);
		UpdateChecked(_checked["Intensity"], SettingsManager.Settings.Intensity);
		UpdateChecked(_checked["Baseline"],  SettingsManager.Settings.Baseline);
	}

	//----------------------------------------------------------------------------------------------------
	private static void UpdateChecked(Transform obj, bool state)
	{
		obj.Find("True").gameObject.SetActive(state);
		obj.Find("False").gameObject.SetActive(!state);
	}

	//----------------------------------------------------------------------------------------------------
	private static void UpdateText(string text = "", bool error = true)
	{
		_errorText.color = error ? ErrorColor : ValidColor;
		_errorText.text  = text;
	}

	//----------------------------------------------------------------------------------------------------
	private void TestList()
	{
		var Test  = new List<List<int>>();
		var nlist = Random.Range(2, 7);
		Debug.Log("Number of List : " + nlist);
		for (var i = 0; i < nlist; ++i) {
			Test.Add(new List<int>());
			var m = Random.Range(2, 7);
			Debug.Log($"Number of element for list {i} : {m}");
			for (var j = 0; j < m; ++j) { Test[i].Add(Random.Range(0, 9)); }
		}

		var s = "Original List : \n";
		for (var i = 0; i < Test.Count; ++i) { s += Test[i].Aggregate($"{i}: ", (current, f) => current + (f + "  ")) + "\n"; }
		Test.Shuffle();
		foreach (var l in Test) { l.Shuffle(); }
		s += "Shuffled List : \n";
		for (var i = 0; i < Test.Count; ++i) { s += Test[i].Aggregate($"{i}: ", (current, f) => current + (f + "  ")) + "\n"; }
		Debug.Log(s);
	}

	#endregion
}
