﻿using System.Collections.Generic;
using System.Linq;
using Math = System.Math;

/// <summary> Add Functions To IList objects. </summary>
public static class IListExtensions
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Shuffles the element order of the list. </summary>
	public static void Shuffle<T>(this IList<T> list)
	{
		var count = list.Count;
		var last  = count - 1;
		for (var i = 0; i < last; ++i) {
			var r = UnityEngine.Random.Range(i, count);
			list.Swap(i, r);
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get The value on this idx but first or last value if idx mismatch. </summary>
	public static T GetValid<T>(this IList<T> list, int idx) { return list[Math.Max(Math.Min(idx, list.Count - 1), 0)]; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Swap two elements of the list. </summary>
	private static void Swap<T>(this IList<T> list, int a, int b)
	{
		var tmp = list[a];
		list[a] = list[b];
		list[b] = tmp;
	}

	//----------------------------------------------------------------------------------------------------
	public static void RangeF(this IList<float> list, float min, float max, int n)
	{
		list.Clear();
		var step = (max - min) / n;
		for (var i = 0; i < n; ++i) { list.Add(min + i * step); }
	}

	//----------------------------------------------------------------------------------------------------
	public static void RangeI(this IList<int> list, int min, int max, int n)
	{
		list.Clear();
		var step = (max - min) / n;
		for (var i = 0; i < n; ++i) { list.Add(min + i * step); }
	}

	//----------------------------------------------------------------------------------------------------
	public static void RangeSpeed(this IList<float> list, Speed s) { list.RangeF(s.Min, s.Max, s.N); }

	//----------------------------------------------------------------------------------------------------
	public static void RangeIntensity(this IList<int> list, Intensity i) { list.RangeI(i.Min, i.Max, i.N); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get The Id of the closest value than expected. </summary>
	public static int GetClosestId(this IList<float> list, float value)
	{
		var diff = list.Select(t => Math.Abs(t - value)).ToList();
		return diff.IndexOf(diff.Min());
	}
}
