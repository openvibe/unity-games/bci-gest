﻿using System.Collections.Generic;
using Math = System.Math;
using Debug = UnityEngine.Debug;
using Stimulations = LSL4Unity.OV.Stimulations;
using CultureInfo = System.Globalization.CultureInfo;

/// <summary> Normalisation methods used to get Median and Mad with a CSV file. </summary>
public static class Normalization
{
	#region Members

	private static TrialState _state     = TrialState.Rest;
	private static int        _startLine = 0;

	private static readonly List<List<float>> Data      = new List<List<float>>();
	private static readonly List<float>       LeftData  = new List<float>();
	private static readonly List<float>       RightData = new List<float>();

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the normalization. </summary>
	public static void GetNormalization()
	{
		Clear();
		LoadCSV();
		ParseData();
		if (LeftData.Count == 0 || RightData.Count == 0) {
			Debug.Log($"Error, no data in at least one side ({RightData.Count} sample on the right side, {LeftData.Count} sample on the left side)");
		}
		else {
			Computes();
			SettingsManager.Settings.ValidNormalization();
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset all members of the class. </summary>
	private static void Clear()
	{
		_state     = TrialState.Rest;
		_startLine = 0;
		Data.Clear();
		LeftData.Clear();
		RightData.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the CSV named <c>Distance.csv</c> for the subject selected in main menu. </summary>
	private static void LoadCSV()
	{
		Debug.Log("Load CSV");
		var reader = new System.IO.StreamReader(System.IO.File.OpenRead(Paths.DistancesPath));
		reader.ReadLine(); // Remove titles
		while (!reader.EndOfStream) {
			var line = reader.ReadLine();
			if (line == null) { continue; }
			var values = line.Split(',');

			var upL   = Math.Abs(float.Parse(values[2], CultureInfo.InvariantCulture));
			var left  = Math.Abs(float.Parse(values[3], CultureInfo.InvariantCulture));
			var xLeft = (left - upL) / (upL + left);

			var xRight  = 0.0F;
			var stimIdx = 4;

			if (values.Length > 5) { // We have more value before stims ?
				var upR   = Math.Abs(float.Parse(values[4], CultureInfo.InvariantCulture));
				var right = Math.Abs(float.Parse(values[5], CultureInfo.InvariantCulture));
				xRight  = (right - upR) / (upR + right);
				stimIdx = 6;
			}


			Data.Add(new List<float> { xLeft, xRight, (int.TryParse(values[stimIdx], out var stim) ? stim : 0) });
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Parses the data loaded (not all datas is used only epochs during feedback). </summary>
	private static void ParseData()
	{
		for (var i = 0; i < Data.Count; ++i) {
			switch ((int) Data[i][2]) {
				case (int) Stimulations.GDF_UP:
				case (int) Stimulations.REST_START:
				case (int) Stimulations.REST_STOP:
				case (int) Stimulations.NON_TARGET:
				case (int) Stimulations.BASELINE_STOP:
					// We are in two case : we begin The rest same time we stoping Left or Right or we stop Rest period
					// Add data from start line to this minus one on correct List
					for (var j = _startLine; j < i - 1; ++j) {
						if (_state == TrialState.Rest || _state == TrialState.Left) { LeftData.Add(Data[j][0]); }
						if (_state == TrialState.Rest || _state == TrialState.Right) { RightData.Add(Data[j][1]); }
					}
					_state     = TrialState.Rest;
					_startLine = i;
					break;

				case (int) Stimulations.GDF_LEFT:
				case (int) Stimulations.GDF_LEFT_HAND_MOVEMENT:
				case (int) Stimulations.LABEL_01:
				case (int) Stimulations.LABEL_02:
				case (int) Stimulations.LABEL_03:
				case (int) Stimulations.LABEL_04:
					_state = TrialState.Left;
					break;
				case (int) Stimulations.GDF_RIGHT:
				case (int) Stimulations.GDF_RIGHT_HAND_MOVEMENT:
					_state = TrialState.Right;
					break;

				case (int) Stimulations.GDF_FEEDBACK_CONTINUOUS:
				case (int) Stimulations.TARGET:
					_startLine = i;
					break;
			}
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Computes the median and MAD and write results on settings. </summary>
	private static void Computes()
	{
		var medianL = LeftData.GetMedian();
		var medianR = RightData.GetMedian();

		for (var i = 0; i < LeftData.Count; ++i) { LeftData[i]   = Math.Abs(medianL - LeftData[i]); }
		for (var i = 0; i < RightData.Count; ++i) { RightData[i] = Math.Abs(medianR - RightData[i]); }

		var madL = LeftData.GetMedian();
		var madR = RightData.GetMedian();

		SettingsManager.Settings.Left.SetNormalization(medianL, madL);
		SettingsManager.Settings.Right.SetNormalization(medianR, madR);

		Debug.Log($"Left Median : {medianL}, MAD : {madL}");
		Debug.Log($"Right Median : {medianR}, MAD : {madR}");
	}

	#endregion
}
