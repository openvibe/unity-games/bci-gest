﻿/// <summary> Little class to initialize all used path (OpenViBE, signals, subjects, files...). </summary>
public static class Paths
{
	#region Members

	private static string _signalPath = "";
	public static  string SubjectPath   { get; private set; } = "";
	public static  string ConfigPath    { get; private set; } = "";
	public static  string DistancesPath { get; private set; } = "";

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	public static void Init()
	{
		var mainPath = System.IO.Directory.GetParent(UnityEngine.Application.dataPath).FullName;
		var ovPath   = System.IO.Path.Combine(mainPath, "OpenViBE");
		_signalPath = System.IO.Path.Combine(ovPath, "Signals");
	}

	//----------------------------------------------------------------------------------------------------
	public static void SetSubjectPath(string id)
	{
		SubjectPath = System.IO.Path.Combine(_signalPath, $"Subject {id}");
		if (!System.IO.Directory.Exists(SubjectPath)) { System.IO.Directory.CreateDirectory(SubjectPath); }
		ConfigPath    = System.IO.Path.Combine(SubjectPath, "config.json");
		DistancesPath = System.IO.Path.Combine(SubjectPath, "Distances.csv");
	}

	#endregion
}
