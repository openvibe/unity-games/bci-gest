using UnityEngine;

//----------------------------------------------------------------------------------------------------
/// <summary> Wristband manager. </summary>
public class Wristband : MonoBehaviour
{
	#region Members

	/// <summary> The color name identifier of shader. </summary>
	private static readonly int ColNameId = Shader.PropertyToID("_Color");

	/// <summary> The possible colors. </summary>
	private readonly Color[] _colors =
	{
		new Color(0.431F, 0.8F,   0.686F, 1.0F),
		new Color(0.459F, 0.714F, 0.737F, 1.0F),
		new Color(0.431F, 0.6F,   0.8F,   1.0F),
		new Color(0.482F, 0.494F, 0.784F, 1.0F),
		new Color(0.549F, 0.431F, 0.8F,   1.0F)
	};

	/// <summary> The material of the wristband. </summary>
	private Material _material = null;

	#endregion

	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Start is called before the first frame update. </summary>
	private void Start()
	{
		_material = GetComponent<Renderer>().material;
		SetColor();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets the color with the color number. </summary>
	/// <param name="n"> The number of the color (between 0 and 5). </param>
	public void SetColor(int n = 0)
	{
		if (n < 0) { n               = 0; }
		if (n >= _colors.Length) { n = _colors.Length - 1; }
		if (_material) { _material.SetColor(ColNameId, _colors[n]); }
	}

	#endregion
}
