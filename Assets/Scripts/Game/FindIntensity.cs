﻿using System;
using System.Collections.Generic;
using Input = UnityEngine.Input;
using KeyCode = UnityEngine.KeyCode;

/// <summary> Class to Define the Behavior when Find Intensity is selected. </summary>
///
/// We use it to calibrate gloves and vibrations.
public class FindIntensity : Mode
{
	#region Members

	#region Messages

	private const string BASIC_INSTRUCTION   = "Appuyez sur 0 pour démarrer, 1 pour diminuer l'intensité, 2 pour valider ou 3 pour augmener l'intensité";
	private const string COUPLE_START        = "Appuyez sur 0 pour démarrer le feedback";
	private const string COUPLE_STOP         = "Appuyez sur 0 pour stopper le feedback";
	private const string COUPLE_VALIDATION   = "Quelle intensité était la plus forte ? (1 ou 2)";
	private const string COUPLE_REPLAY       = "Souhaitez vous recommencer (1) changer le Min et Max (2) ou valider (3)? ";
	private const string INTENSITY_UPGRADE   = "Augmentaion de l'intensité";
	private const string INTENSITY_DOWNGRADE = "Diminution de l'intensité";
	private const string INTENSITY_TOO_HIGH  = "Intensité déjà au maximum";
	private const string INTENSITY_TOO_LOW   = "Intensité déjà au minimum";

	#endregion

	/// <summary> Steps of the process. </summary>
	private enum Steps { Start, FindMinLeft, FindMaxLeft, TestLeft, FindMinRight, FindMaxRight, TestRight, Finish }

	/// <summary> Steps of the test process. </summary>
	private enum TestSteps { Start, StartFirst, StopFirst, StartSecond, StopSecond, MakeChoice, Finish }

	//private const int NB_STEPS = 5;   // Number of Step fixed to 5
	private const int MIN_I  = 50;  // Minimum possible fixed to 50
	private const int MAX_I  = 200; // Maximum possible fixed to 200
	private const int STEP_I = 10;  // différence between 2 intensity (for find min and max)

	private Steps _step         = Steps.Start;
	private bool  _instructions = true; // True to show instruction, false otherwise.
	private bool  _side         = true; // True for Left, false for Right.
	private bool  _limit        = true; // True for Min, false for Max.
	private int   _value        = MIN_I;


	private TestSteps _testStep      = TestSteps.Start;
	private int       _currentCouple = 0;
	private int       _element       = 0;
	private int       _performance   = 0;

	private readonly List<List<int>> _couples = new List<List<int>>();

	#endregion

	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Init()
	{
		ResetMembers();
		base.Init();
		Game.SetActiveUI(false, false, false, false, false, true, true);
		SetInstruction("");
		SetInformation("");
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update()
	{
		base.Update();
		if (_step == Steps.Finish) { Quit(); }
		if (_step == Steps.Start) { ChangeStep(Steps.FindMinLeft); }
		if (_step == Steps.TestLeft || _step == Steps.TestRight) { TestValues(); }
		else { FindValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	protected override void Quit()
	{
		SettingsManager.Settings.ValidIntensity();
		base.Quit();
	}

	#endregion

	#region Find Values

	//----------------------------------------------------------------------------------------------------
	/// <summary> Process where one increases/decreases the intensity of the vibrations of a motor for the min or max intensities of this one. </summary>
	private void FindValue()
	{
		// New instruction must be shown
		if (_instructions) {
			_instructions = false;
			_value        = _limit ? MIN_I : MAX_I;
			SetInstruction($"Recherche de l'intensité {LimitStr()} pour la main {SideStr()}.");
			SetInformation(BASIC_INSTRUCTION);
		}

		//Detect the key pressed by the user
		// 0 : Start, 1 : decrease, 2 : validate, 3 : increase
		if (Input.GetKeyDown(KeyCode.Keypad0)) { Arduino.ChangeCurrentIntensity(_value, _side); }
		else if (Input.GetKeyDown(KeyCode.Keypad1)) {
			if (_value < MAX_I) {
				_value += STEP_I;
				SetInformation(INTENSITY_UPGRADE);
				Arduino.ChangeCurrentIntensity(_value, _side);
			}
			else { SetInformation(INTENSITY_TOO_HIGH); }
		}
		else if (Input.GetKeyDown(KeyCode.Keypad3)) {
			if (_value > MIN_I) {
				_value -= STEP_I;
				SetInformation(INTENSITY_DOWNGRADE);
				Arduino.ChangeCurrentIntensity(_value, _side);
			}
			else { SetInformation(INTENSITY_TOO_LOW); }
		}
		else if (Input.GetKeyDown(KeyCode.Keypad2)) {
			Arduino.StopMotors();
			_instructions = true;
			var intensity = _side ? SettingsManager.Settings.Left.Intensity : SettingsManager.Settings.Right.Intensity;
			if (_limit) { intensity.Min = _value; }
			else { intensity.Max        = _value; }
			SetInformation($"Intensité {LimitStr()} pour la main {SideStr()} trouvé.");
			ChangeStep(NextStep());
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Test the range between min and max selected (with 5 intensity steps). </summary>
	private void TestValues()
	{
		var intensity = _step == Steps.TestLeft
							? SettingsManager.Settings.Left.Intensity
							: SettingsManager.Settings.Right.Intensity; // in C# = pass reference for classes
		//Compute the different value for a given number of thresholds
		if (_instructions) {
			_testStep     = TestSteps.Start;
			_instructions = false;
			CreateRandomCouples(intensity);
			_currentCouple = 0;
			_element       = 0;
			_performance   = 0;
			SetInstruction($"Test des intensités pour la main {SideStr()}.\n");
			SetInformation(COUPLE_START + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
		}

		// Course of the test :
		// For all couples
		// Press 0 to start first intensity
		// Press 0 to stop first intensity
		// Press 0 to start second intensity
		// Press 0 to stop second intensity
		// Press 1 or 2 to select the highest intensity

		switch (_testStep) {
			case TestSteps.Start:
				if (Input.GetKeyDown(KeyCode.Keypad0)) {
					_testStep = TestSteps.StartFirst;
					Arduino.ChangeCurrentIntensity(_couples[_currentCouple][_element], _side);
					SetInformation(COUPLE_STOP + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
				}
				break;

			case TestSteps.StartFirst:
				if (Input.GetKeyDown(KeyCode.Keypad0)) {
					_testStep = TestSteps.StopFirst;
					Arduino.StopMotors();
					_element = 1;
					SetInformation(COUPLE_START + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
				}
				break;

			case TestSteps.StopFirst:
				if (Input.GetKeyDown(KeyCode.Keypad0)) {
					_testStep = TestSteps.StartSecond;
					Arduino.ChangeCurrentIntensity(_couples[_currentCouple][_element], _side);
					SetInformation(COUPLE_STOP + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
				}
				break;

			case TestSteps.StartSecond:
				if (Input.GetKeyDown(KeyCode.Keypad0)) {
					_testStep = TestSteps.StopSecond;
					Arduino.StopMotors();
					SetInformation(COUPLE_VALIDATION);
				}
				break;

			case TestSteps.StopSecond:
				if (Input.GetKeyDown(KeyCode.Keypad1)) {
					_testStep = TestSteps.MakeChoice;
					if (_couples[_currentCouple][0] >= _couples[_currentCouple][1]) { _performance++; }
				}
				else if (Input.GetKeyDown(KeyCode.Keypad2)) {
					_testStep = TestSteps.MakeChoice;
					if (_couples[_currentCouple][1] >= _couples[_currentCouple][0]) { _performance++; }
				}
				break;

			case TestSteps.MakeChoice:
				if (_currentCouple < _couples.Count - 1) {
					_testStep = TestSteps.Start;
					_element  = 0;
					_currentCouple++;
					SetInformation(COUPLE_START + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
				}
				else {
					_testStep = TestSteps.Finish;
					SetInformation($"Performance: {_performance} / {_couples.Count}.\n" + COUPLE_REPLAY);
				}
				break;

			case TestSteps.Finish:
				if (Input.GetKeyDown(KeyCode.Keypad1)) {
					_element       = 0;
					_currentCouple = 0;
					SetInformation(COUPLE_START + $" {_element + 1} du couple {_currentCouple + 1}/{_couples.Count}.");
				}
				else if (Input.GetKeyDown(KeyCode.Keypad2)) {
					if (_step == Steps.TestLeft) { ChangeStep(Steps.FindMinLeft); }
					if (_step == Steps.TestRight) { ChangeStep(Steps.FindMinRight); }
				}
				else if (Input.GetKeyDown(KeyCode.Keypad3)) { ChangeStep(NextStep()); }
				break;
			default: throw new ArgumentOutOfRangeException();
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Assure the order of the process (we can use the int value of enum but in case of modification in enum, it's safer). </summary>
	/// <returns> the next step. </returns>
	/// <exception cref="System.ArgumentOutOfRangeException"></exception>
	private Steps NextStep()
	{
		switch (_step) {
			case Steps.Start:        return Steps.FindMinLeft;
			case Steps.FindMinLeft:  return Steps.FindMaxLeft;
			case Steps.FindMaxLeft:  return Steps.TestLeft;
			case Steps.TestLeft:     return Steps.FindMinRight;
			case Steps.FindMinRight: return Steps.FindMaxRight;
			case Steps.FindMaxRight: return Steps.TestRight;
			case Steps.TestRight:    return Steps.Finish;
			case Steps.Finish:       return Steps.Finish;
			default:                 throw new ArgumentOutOfRangeException();
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Changes the step, and set the member to know on witch side we are and if we check the min or max). </summary>
	/// <param name="step"> The new step. </param>
	/// <exception cref="System.ArgumentOutOfRangeException"></exception>
	private void ChangeStep(Steps step)
	{
		_step         = step;
		_instructions = true;
		switch (_step) {
			case Steps.Start: break;
			case Steps.FindMinLeft:
				_side  = true;
				_limit = true;
				break;
			case Steps.FindMaxLeft:
				_side  = true;
				_limit = false;
				break;
			case Steps.TestLeft:
				_side = true;
				break;
			case Steps.FindMinRight:
				_side  = false;
				_limit = true;
				break;
			case Steps.FindMaxRight:
				_side  = false;
				_limit = false;
				break;
			case Steps.TestRight:
				_side = false;
				break;
			case Steps.Finish: break;
			default:           throw new ArgumentOutOfRangeException();
		}
	}

	#endregion

	#region Utils

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	protected override void ResetMembers()
	{
		base.ResetMembers();
		ChangeStep(Steps.Start);
		_testStep = TestSteps.Start;
		ResetSetting();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Creates the random couples to test and find thresholds. </summary>
	/// <param name="intensity"> The intensity setting. </param>
	private void CreateRandomCouples(Intensity intensity)
	{
		_couples.Clear();
		var tmp = new List<int>();
		tmp.RangeI(intensity.Min, intensity.Max, intensity.N + 1);
		for (var j = 0; j < tmp.Count - 1; j++) { _couples.Add(new List<int> { tmp[j], tmp[j + 1] }); }
		_couples.Shuffle();
		for (var k = 0; k < _couples.Count - 1; k++) { _couples[k].Shuffle(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the Setting in default values. </summary>
	private void ResetSetting()
	{
		SettingsManager.Settings.Left.Intensity  = new Intensity();
		SettingsManager.Settings.Right.Intensity = new Intensity();
	}

	#endregion

	#region UI

	//----------------------------------------------------------------------------------------------------
	/// <summary> Method to Display main Instruction (always display as "we test min for left motors"). </summary>
	/// <param name="msg"> Message to display. </param>
	private void SetInstruction(string msg)
	{
		UnityEngine.Debug.Log(msg);
		Game.SetInstructiontUI(msg);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Method to Display Instruction (this instruction change often as coneseuqence of key pressed). </summary>
	/// <param name="msg"> Message to display. </param>
	private void SetInformation(string msg)
	{
		UnityEngine.Debug.Log(msg);
		Game.SetInformationUI(msg);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Quick method to get min/max string with _limit setting. </summary>
	private string LimitStr() { return _limit ? "Min" : "Max"; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Quick method to get left/right string with _side setting. </summary>
	private string SideStr() { return _side ? "Gauche" : "Droite"; }

	#endregion
}
