///-------------------------------------------------------------------------------------------------
///
/// \file StimulationInlet.cs
/// \brief Implementation for an Inlet receiving Stimulations (int) from OpenViBE.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------
/// <summary> Implementation for an Inlet receiving Stimulations (int) from OpenViBE. </summary>
/// <seealso cref="OVIntInlet" />
public class StimulationInlet : LSL4Unity.OV.OVIntInlet
{
	public double Time { get; private set; } = 0;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Member that contains the last sample. </summary>
	/// <value> The last sample. </value>
	public int[] LastSample { get; private set; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Process when samples are available. </summary>
	/// <inheritdoc />
	protected override void Process(int[] input, double time)
	{
		LastSample = input;
		Time       = time;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines whether the sample buffer is filled. </summary>
	/// <returns> <c>true</c> if this instance is filled; otherwise, <c>false</c>. </returns>
	public bool IsFilled() { return LastSample != null && LastSample.Length > 0; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines if the stream have new information only with previous process and specified time. </summary>
	/// <param name="time"> The time. </param>
	/// <returns> <c>true</c> if the stream is update; otherwise, <c>false</c>. </returns>
	public bool IsUpdate(double time) { return System.Math.Abs(Time - time) >= LSL4Unity.OV.Constants.TOLERANCE; }
}
