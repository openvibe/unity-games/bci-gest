﻿/// <summary> Class to Define the Behavior when Baseline is selected (old possibility not used actually). </summary>
public class Baseline : Mode
{
	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update()
	{
		base.Update();
		if (Finish) { Quit(); }
		if (!Game.StimInlet.IsFilled() || !Game.StimInlet.IsUpdate(Game.LastStimTime)) { return; }
		Game.LastStimTime = Game.StimInlet.Time;
		var value = Game.StimInlet.LastSample[0];
		ChangeState(value);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	protected override void Quit()
	{
		SettingsManager.Settings.ValidBaseline();
		base.Quit();
	}

	#endregion
}
