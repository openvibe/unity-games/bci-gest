﻿using Math = System.Math;
using Debug = UnityEngine.Debug;
using System.Collections.Generic;

public class Online : Mode
{
	#region Members

	/// <summary> Windows (epochs) counter for statistics. </summary>
	private static int[,] _nWindows =
	{
		{ 0, 0 }, // Left recognize / number
		{ 0, 0 }, // Right recognize / number
		{ 0, 0 }, // Ball recognize / number
		{ 0, 0 }, // Fork recognize / number
		{ 0, 0 }, // Circle recognize / number
		{ 0, 0 }  // Nut recognize / number
	};

	#endregion

	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Init()
	{
		ResetMembers();
		base.Init();
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update()
	{
		base.Update();
		if (Finish) { Quit(); }
		// If Stimulation is received
		if (Game.StimInlet.IsFilled() && Game.StimInlet.IsUpdate(Game.LastStimTime)) {
			Game.LastStimTime = Game.StimInlet.Time;
			var value = Game.StimInlet.LastSample[0];
			ChangeState(value);
		}

		var sample = Game.SignalInlet.LastSample;
		if (!Feedback || !Game.SignalInlet.IsFilled() || !Game.SignalInlet.IsUpdate(Game.LastSignalTime) || sample.Length < 2) { return; }
		if (State == TrialState.Rest) { return; }
		Game.LastSignalTime = Game.SignalInlet.Time;

		var side      = SettingsManager.Settings.Left;
		var threshold = ThresholdsLeftI;
		var wristband = Game.WristbandLeft;
		if (State == TrialState.Right) {
			side      = SettingsManager.Settings.Right;
			threshold = ThresholdsRightI;
			wristband = Game.WristbandRight;
		}

		var perf      = GetPerf(sample[0], sample[1], side.Median, side.MAD);
		var index     = -1;
		var intensity = -1;
		GetValues(perf, threshold, (int) State - 1, ref index, ref intensity);
		//Debug.Log($"Perf : {perf}, increment : {increment}, speed : {speed}, intensity : {intensity}");
		Arduino.ChangeCurrentIntensity(intensity < 0 ? 0 : intensity, State != TrialState.Right);
		Game.Anim.speed = 1;
		wristband.SetColor(index); // Change Wristband color with index variable
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	protected override void Quit()
	{
		string[] states = { "Left", "Right", "Ball", "Fork", "Circle", "Nut" };
		for (var i = 0; i < _nWindows.GetLength(0); i++) {
			if (_nWindows[i, 1] == 0) { continue; }
			Debug.Log($"Mean performance {states[i]}: {_nWindows[i, 0]} / {_nWindows[i, 1]} ({100.0F * _nWindows[i, 0] / _nWindows[i, 1]} %)");
		}

		base.Quit();
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	protected override void ResetMembers()
	{
		base.ResetMembers();
		_nWindows = new[,] { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };
	}

	#endregion

	#region Private Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the performance biased by median and MAD (Median Absolute Deviation). </summary>
	/// <param name="p1"> The first distance (normally Rest). </param>
	/// <param name="p2"> The second distance (normally Left or Right). </param>
	/// <param name="median"> The median. </param>
	/// <param name="mad"> The Median Absolute Deviation. </param>
	/// <returns> Performance biased. </returns>
	private static float GetPerf(float p1, float p2, float median, float mad)
	{
		return ((Math.Abs(p2) - Math.Abs(p1)) / (Math.Abs(p2) + Math.Abs(p1)) - median) / (mad * 2);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the values (index of color and intensity of motors) with the designed performance. </summary>
	/// <param name="perf"> The performance. </param>
	/// <param name="thresholds"> The intensity thresholds. </param>
	/// <param name="state"> The state (Left, Right, Ball, Fork, Circle, Nut). </param>
	/// <param name="index"> The index of color. </param>
	/// <param name="intensity"> The intensity of motors. </param>
	private static void GetValues(float perf, List<int> thresholds, int state, ref int index, ref int intensity)
	{
		var increment = ToSave ? 1 : 0;
		_nWindows[state, 1] += increment;
		if (perf < 0) { return; }
		_nWindows[state, 0] += increment;
		index               =  (int) (perf * thresholds.Count); // Perf is between 0 and 1 in most cases.
		intensity           =  thresholds.GetValid(index);      // In case we assure index is valid
	}

	#endregion
}
