﻿using System.Collections.Generic;
using Stimulations = LSL4Unity.OV.Stimulations;

public class Mode
{
	#region Members

	protected static bool       Finish   = false;
	protected static bool       Feedback = false;
	protected static bool       ToSave   = false;
	protected static TrialState State    = TrialState.Rest;

	protected static readonly List<int> ThresholdsLeftI  = new List<int>(); // Intensity Thresholds Left
	protected static readonly List<int> ThresholdsRightI = new List<int>(); // Intensity Thresholds Right

	#endregion

	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the game mode. </summary>
	public virtual void Init()
	{
		ResetMembers();
		ThresholdsLeftI.RangeIntensity(SettingsManager.Settings.Right.Intensity);
		ThresholdsRightI.RangeIntensity(SettingsManager.Settings.Right.Intensity);
		Game.DeactivateUI();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Behavior When <c>Update</c> is called (on each <c>Update</c> in <c>Game</c> Script). </summary>
	/// <remarks> Escape is used for all modes to close the game and return to the main menu. </remarks>
	public virtual void Update()
	{
		if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Escape)) { Finish = true; }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Behavior When <c>Quit</c> is called (when the boolean <c>Finish</c> is set to <c>true</c>, <c>Update</c> call <c>Quit</c>). </summary>
	protected virtual void Quit()
	{
		Finish = true;
		UnityEngine.SceneManagement.SceneManager.LoadScene("Start Menu");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the members in default values. </summary>
	protected virtual void ResetMembers()
	{
		SetState(TrialState.Rest);
		Finish   = false;
		Feedback = false;
		ToSave   = false;
		ThresholdsLeftI.Clear();
		ThresholdsRightI.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Changes the state of members, UI and feedback (visual and tactile) with the specified stimulation. </summary>
	/// <param name="stim"> The stimulation. </param>
	/// <returns> <c>True</c> if stimulation is recognized, <c>False</c> otherwise. </returns>
	/// <exception cref="System.ArgumentOutOfRangeException"></exception>
	protected static bool ChangeState(int stim)
	{
		switch (stim) {
			case (int) Stimulations.GDF_UP:
			case (int) Stimulations.REST_START:
			case (int) Stimulations.NON_TARGET:
				ToSave = false;
				Game.SetActiveUI(true, false, false, true, false);
				Game.SetTextUI("Repos");
				Arduino.StopMotors();
				SetState(TrialState.Rest);
				break;

			case (int) Stimulations.GDF_LEFT:
			case (int) Stimulations.GDF_LEFT_HAND_MOVEMENT:
				Game.SetActiveUI(true, true, false, false, false);
				Arduino.StopMotors();
				SetState(TrialState.Left);
				break;

			case (int) Stimulations.GDF_RIGHT:
			case (int) Stimulations.GDF_RIGHT_HAND_MOVEMENT:
				Game.SetActiveUI(true, false, true, false, false);
				Arduino.StopMotors();
				SetState(TrialState.Right);
				break;

			case (int) Stimulations.LABEL_01:
				Game.SetActiveUI();
				Arduino.StopMotors();
				SetState(TrialState.Ball);
				break;

			case (int) Stimulations.LABEL_02:
				Game.SetActiveUI();
				Arduino.StopMotors();
				SetState(TrialState.Fork);
				break;

			case (int) Stimulations.LABEL_03:
				Game.SetActiveUI();
				Arduino.StopMotors();
				SetState(TrialState.Circle);
				break;

			case (int) Stimulations.LABEL_04:
				Game.SetActiveUI();
				Arduino.StopMotors();
				SetState(TrialState.Nut);
				break;

			case (int) Stimulations.GDF_END_OF_TRIAL:
			case (int) Stimulations.TRIAL_STOP:
				Game.SetActiveUI();
				Arduino.StopMotors();
				Feedback = false;
				SetState(TrialState.Rest);
				break;

			case (int) Stimulations.GDF_START_OF_TRIAL:
			case (int) Stimulations.EXPERIMENT_START:
				Game.SetActiveUI(true);
				break;

			case (int) Stimulations.GDF_END_OF_SESSION:
			case (int) Stimulations.EXPERIMENT_STOP:
				Game.SetActiveUI(false, false, false, false, true);
				Arduino.StopMotors();
				Finish = true;
				break;

			case (int) Stimulations.GDF_FEEDBACK_CONTINUOUS:
			case (int) Stimulations.TARGET:
				Game.SetActiveUI();
				Feedback = true;
				ToSave   = true;
				break;

			default: return false;
		}
		return true;
	}

	#endregion

	#region Private Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> When state is changed, we reset animation, speed and color. </summary>
	/// <param name="state"></param>
	private static void SetState(TrialState state)
	{
		State = state;
		Game.SetAnim(Game.GetAnim(state)); // Set the right animation
		Game.SetSpeed(0);                  // Reset speed to 0
		Game.WristbandLeft.SetColor();     // Reset wristband left color
		Game.WristbandRight.SetColor();    // Reset wristband right color
	}

	#endregion
}
