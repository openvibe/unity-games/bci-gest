﻿/// <summary> Class to Define the Behavior when Baseline is selected. </summary>
public class AnimationTest : Mode
{
	#region Members

	/// <summary> The current color. </summary>
	private int _color = 0;

	#endregion

	#region Base Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Init()
	{
		base.Init();
		Game.SetSpeed(1.0F);
		Game.SetActiveUI(false, false, false, true);
		Game.SetTextUI("Press Num Key for Animation : Rest (0), Grasping Left (1), Grasping Right (2), Ball (3), Fork (4), Circle (5), Nut (6), Increase color (up), Decrease color (down)");
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update()
	{
		base.Update();
		if (Finish) { Quit(); }
		// Kayboard management
		if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad0)) { Game.SetAnim("Rest"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad1)) { Game.SetAnim("Grasping Left"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad2)) { Game.SetAnim("Grasping Right"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad3)) { Game.SetAnim("Ball"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad4)) { Game.SetAnim("Fork"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad5)) { Game.SetAnim("Circle"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.Keypad6)) { Game.SetAnim("Nut"); }
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.UpArrow)) {
			_color++;
			if (_color >= 5) { _color = 0; }
			Game.WristbandLeft.SetColor(_color);
			Game.WristbandRight.SetColor(_color);
			UnityEngine.Debug.Log($"Color : {_color}");
		}
		else if (UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.DownArrow)) {
			_color--;
			if (_color < 0) { _color = 4; }
			UnityEngine.Debug.Log($"Color : {_color}");
			Game.WristbandLeft.SetColor(_color);
			Game.WristbandRight.SetColor(_color);
		}
	}

	#endregion
}
