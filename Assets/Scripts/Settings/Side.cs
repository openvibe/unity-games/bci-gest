﻿/// <summary> Class For Side Setting, we store motor's range Intensity, median and MAD. </summary>
[System.Serializable] public class Side
{
	#region Members

	public Intensity Intensity = new Intensity();
	public float     Median    = 0.0F;
	public float     MAD       = 0.0F; // Median Absolute Deviation

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	public Side(float median, float mad)
	{
		Median = median;
		MAD    = mad;
	}

	//----------------------------------------------------------------------------------------------------
	public void SetNormalization(float median, float mad)
	{
		Median = median;
		MAD    = mad;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return "[Tactile Intensity: " + Intensity + ", Median: " + Median + ", MAD: " + MAD + "]"; }

	#endregion
}
