﻿using JsonUtility = UnityEngine.JsonUtility;
using Debug = UnityEngine.Debug;

/// <summary> Possible Game Mode (Baseline is now useless). </summary>
public enum GameMode { FindIntensity, Baseline, Online, AnimationTest }

/// <summary> Setting Manager used to check wich mode we cna start, save and load subject settings. </summary>
public class Settings
{
	#region Members

	public static GameMode Mode = GameMode.FindIntensity;

	private string _subjectID = "0";

	public bool Checked   = false;
	public bool Intensity = false;
	public bool Baseline  = false;

	public Side Left  = new Side(0.01995F, 0.00375F);
	public Side Right = new Side(0.01995F, 0.00375F);

	#endregion

	#region Management

	//----------------------------------------------------------------------------------------------------
	public static void Init() { Paths.Init(); }

	//----------------------------------------------------------------------------------------------------
	public void ChangeID(string s) { _subjectID = s; }

	//----------------------------------------------------------------------------------------------------
	public void CheckSubject()
	{
		Paths.SetSubjectPath(_subjectID);
		Load();
		Checked = true;
		Save();
	}

	//----------------------------------------------------------------------------------------------------
	public void ValidIntensity()
	{
		Intensity = true;
		Save();
	}

	//----------------------------------------------------------------------------------------------------
	public void ValidBaseline()
	{
		Baseline = true;
		Save();
	}

	//----------------------------------------------------------------------------------------------------
	public void ValidNormalization() { Save(); }

	#endregion

	#region Checker

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines if we can check the intensity. </summary>
	/// <returns> <c>true</c> if we can check the intensity, <c>false</c> otherwise. </returns>
	public bool CanCheckIntensity() { return Checked; }

	//----------------------------------------------------------------------------------------------------
	// <summary> Determines if we can make the baseline. </summary>
	// <returns> <c>true</c> if we can make the baseline, <c>false</c> otherwise. </returns>
	// <remarks> Intensity is not required if we haven't gloves. </remarks>
	//public bool CanMakeBaseline() { return CanCheckIntensity(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines if we can make a run. </summary>
	/// <returns> <c>true</c> if we can make a run, <c>false</c> otherwise. </returns>
	public bool CanMakeRun() { return CanCheckIntensity(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines if we can make a normalization. </summary>
	/// <returns> <c>true</c> if we can make a normalization, <c>false</c> otherwise. </returns>
	public bool CanMakeNormalization() { return Checked && System.IO.File.Exists(Paths.DistancesPath); }

	#endregion

	#region Save/Load

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the config File of Subject. </summary>
	private void Save()
	{
		Debug.Log($"Save {Paths.ConfigPath}...");
		Debug.Log(JsonUtility.ToJson(this,                                     true));
		System.IO.File.WriteAllText(Paths.ConfigPath, JsonUtility.ToJson(this, true));
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the config File of Subject if exist. </summary>
	private void Load()
	{
		Debug.Log($"Load {Paths.ConfigPath}...");
		if (!System.IO.File.Exists(Paths.ConfigPath)) { return; }
		JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(Paths.ConfigPath), this);
	}

	//----------------------------------------------------------------------------------------------------
	public override string ToString() { return ""; }

	#endregion
}
