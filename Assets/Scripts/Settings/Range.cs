﻿/// <summary> Class For Range Setting. </summary>
[System.Serializable] public abstract class Range<T> where T : System.IComparable
{
	public T   Min;
	public T   Max;
	public int N;

	protected Range(T min, T max, int n)
	{
		Min = min;
		Max = max;
		N   = n;
	}

	public abstract bool HaveMin();
	public abstract bool HaveMax();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return "[min: " + Min + ", max: " + Max + ", nThreshold: " + N + "]"; }
}

//----------------------------------------------------------------------------------------------------
/// <inheritdoc />
/// <summary> Range Class For Visual Speed Setting. </summary>
[System.Serializable] public class Speed : Range<float>
{
	public Speed(float min = -1, float max = -1, int n = 5) : base(min, max, n) { }
	public override bool HaveMin() { return Min >= 0; }
	public override bool HaveMax() { return Max >= 0; }
}

//----------------------------------------------------------------------------------------------------
/// <inheritdoc />
/// <summary> Range Class For Tactile Intensity Setting. </summary>
[System.Serializable] public class Intensity : Range<int>
{
	public Intensity(int min = -1, int max = -1, int n = 5) : base(min, max, n) { }
	public override bool HaveMin() { return Min >= 0; }
	public override bool HaveMax() { return Max >= 0; }
}
