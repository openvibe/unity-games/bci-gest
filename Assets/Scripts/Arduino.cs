﻿using SerialPort = System.IO.Ports.SerialPort;
using UnityEngine;

/// <summary> Classe to manage Arduino communication (for gloves). </summary>
public class Arduino : MonoBehaviour
{
	#region Members

	private static Arduino _instance = null;

	private const int BAUDRATE = 9600;

	public static  string     ComPort { get; private set; } = "No Port";
	private static SerialPort _stream = null;

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	private void Awake()
	{
		if (_instance != null) { Destroy(gameObject); }
		else {
			_instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	//----------------------------------------------------------------------------------------------------
	private void OnDestroy() { Close(); }

	#endregion

	#region Getter / Setter

	//----------------------------------------------------------------------------------------------------
	public static void ChangePort(string s) { ComPort = s; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the COM port list. </summary>
	/// <returns> List of possible port (store in a string). </returns>
	/// <remarks> We only retrieve the port identifier and not the name of the connected object. It may be necessary to go through the device manager to find the correct port. </remarks>
	public static string[] GetComPortList() { return SerialPort.GetPortNames(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the COM port. </summary>
	/// <remarks> We set COM port to the first port found. In debug list of port is written. </remarks>
	public static void GetComPort()
	{
		// Get a list of serial port names.
		var ports = GetComPortList();
		ChangePort(ports.Length != 0 ? ports[0] : "No Port");

		// Display each port name to the Log.
		Debug.Log("The following serial ports were found:");
		foreach (var port in ports) { Debug.Log(port); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Check if the specified port exist. </summary>
	/// <param name="port"> The port. </param>
	/// <returns><c>true</c> if the port exists, <c>false</c> otherwise. </returns>
	public static bool Exist(string port)
	{
		var found = false;
		var ports = GetComPortList();
		foreach (var s in ports) {
			if (port == s) { found = true; }
		}
		return found;
	}

	#endregion

	#region Serial Communication

	//----------------------------------------------------------------------------------------------------
	/// <summary> Opens a stream to Arduino. </summary>
	public static void Open()
	{
		if (!Exist(ComPort)) { return; }
		_stream = new SerialPort(ComPort, BAUDRATE);
		if (_stream.IsOpen) { return; }
		_stream.Open();
		_stream.ReadTimeout = 100; // Avoid Freeze during reading Serial.
		//stream.DtrEnable = true;
		//stream.RtsEnable = true;
		if (_stream.IsOpen) { Debug.Log($"Open stream {ComPort} with Baudrate {BAUDRATE}"); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Closes the stream to the arduino after stopping the motors. </summary>
	public static void Close()
	{
		if (_stream == null || !_stream.IsOpen) { return; }
		StopMotors();
		_stream.Close();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reads the line send by the Arduino. </summary>
	public static string ReadLine()
	{
		if (_stream == null || !_stream.IsOpen) { return ""; }
		try { return _stream.ReadLine(); }
		catch (System.TimeoutException) { return ""; }
	}

	//----------------------------------------------------------------------------------------------------
	//public static void ActiveLeft() { Write("g0"); }

	//----------------------------------------------------------------------------------------------------
	//public static void ActiveRight() { Write("g1"); }

	//----------------------------------------------------------------------------------------------------
	//public static void UpdateIntensity(int intensity) { Write("i" + intensity); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sends the message to manage motors. </summary>
	public static void ChangeCurrentIntensity(int intensity, bool left) { Write("g" + (left ? "0" : "1") + "i" + intensity); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sends the message to stop motors. </summary>
	public static void StopMotors() { Write("a"); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sends a line to the Arduino. </summary>
	private static void Write(string msg)
	{
		if (_stream == null || !_stream.IsOpen) { return; }
		_stream.WriteLine(msg);
	}

	#endregion
}
