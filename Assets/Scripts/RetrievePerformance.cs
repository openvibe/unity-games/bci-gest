﻿using System;
using System.Collections.Generic;
using Math = System.Math;
using Debug = UnityEngine.Debug;
using Stimulations = LSL4Unity.OV.Stimulations;
using CultureInfo = System.Globalization.CultureInfo;

/// <summary> Class used to retrieve performance of a run with a CSV file. </summary>
public static class RetrievePerformance
{
	#region Members

	private static TrialState _state            = TrialState.Rest;
	private static int        _startLine        = 0;
	private static int        _nRest            = 0;
	private static int        _nLeft            = 0;
	private static int        _nRight           = 0;
	private static int        _nLeftRecognized  = 0;
	private static int        _nRightRecognized = 0;

	private static readonly List<List<float>> Data = new List<List<float>>();

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the performance. </summary>
	public static string GetPerformance()
	{
		Clear();
		LoadCSV();
		return Computes();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset all members of the class. </summary>
	private static void Clear()
	{
		_state            = TrialState.Rest;
		_startLine        = 0;
		_nRest            = 0;
		_nLeft            = 0;
		_nRight           = 0;
		_nLeftRecognized  = 0;
		_nRightRecognized = 0;
		Data.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the CSV named <c>Distance.csv</c> for the subject selected in main menu. </summary>
	private static void LoadCSV()
	{
		Debug.Log("Load CSV");
		var reader = new System.IO.StreamReader(System.IO.File.OpenRead(Paths.DistancesPath));
		reader.ReadLine(); // Remove titles
		while (!reader.EndOfStream) {
			var line = reader.ReadLine();
			if (line == null) { continue; }
			var values = line.Split(',');

			var upL     = Math.Abs(float.Parse(values[2], CultureInfo.InvariantCulture));
			var left    = Math.Abs(float.Parse(values[3], CultureInfo.InvariantCulture));
			var perfL   = ((left - upL) / (upL + left) - SettingsManager.Settings.Left.Median) / (SettingsManager.Settings.Left.MAD * 2);
			var perfR   = 0.0F;
			var stimIdx = 4;
			if (values.Length > 5) { // We have more value before stims ?
				var upR   = Math.Abs(float.Parse(values[4], CultureInfo.InvariantCulture));
				var right = Math.Abs(float.Parse(values[5], CultureInfo.InvariantCulture));
				perfR   = ((right - upR) / (upR + right) - SettingsManager.Settings.Right.Median) / (SettingsManager.Settings.Right.MAD * 2);
				stimIdx = 6;
			}

			Data.Add(new List<float> { perfL, perfR, (int.TryParse(values[stimIdx], out var stim) ? stim : 0) });
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Parses the data loaded (not all datas is used only epochs during feedback) and count the windows (epochs) recognized or not. </summary>
	private static string Computes()
	{
		for (var i = 0; i < Data.Count; ++i) {
			switch ((int) Data[i][2]) {
				case (int) Stimulations.GDF_UP:
				case (int) Stimulations.REST_START:
				case (int) Stimulations.REST_STOP:
				case (int) Stimulations.NON_TARGET:
				case (int) Stimulations.BASELINE_STOP:
					// Add data from start line to this minus one on correct List
					for (var j = _startLine; j < i - 1; ++j) {
						switch (_state) {
							case TrialState.Rest:
							{
								_nRest++;
								if (Data[j][0] < 0) { _nLeftRecognized++; }
								if (Data[j][1] < 0) { _nRightRecognized++; }
								break;
							}

							case TrialState.Ball:
							case TrialState.Fork:
							case TrialState.Circle:
							case TrialState.Nut:
							case TrialState.Left:
							{
								_nLeft++;
								if (Data[j][0] > 0) { _nLeftRecognized++; }
								break;
							}
							case TrialState.Right:
							{
								_nRight++;
								if (Data[j][1] > 0) { _nRightRecognized++; }
								break;
							}
							default: throw new ArgumentOutOfRangeException();
						}
					}

					_state     = TrialState.Rest;
					_startLine = i;
					break;

				case (int) Stimulations.GDF_LEFT:
				case (int) Stimulations.GDF_LEFT_HAND_MOVEMENT:
				case (int) Stimulations.LABEL_01:
				case (int) Stimulations.LABEL_02:
				case (int) Stimulations.LABEL_03:
				case (int) Stimulations.LABEL_04:
					_state = TrialState.Left;
					break;

				case (int) Stimulations.GDF_RIGHT:
				case (int) Stimulations.GDF_RIGHT_HAND_MOVEMENT:
					_state = TrialState.Right;
					break;

				case (int) Stimulations.GDF_FEEDBACK_CONTINUOUS:
				case (int) Stimulations.TARGET:
					_startLine = i;
					break;
			}
		}

		var totalL    = _nLeft + _nRest;
		var totalR    = _nRight + _nRest;
		var meanPerfL = totalL != 0 ? (float) (100.0 * _nLeftRecognized) / totalL : 0.0F;
		var meanPerfR = totalR != 0 ? (float) (100.0 * _nRightRecognized) / totalR : 0.0F;
		var res       = $"Left : {_nLeftRecognized} / {totalL} ({meanPerfL:F2} %)\nRight : {_nRightRecognized} / {totalR} ({meanPerfR:F2} %)";

		Debug.Log($"Number of Left Windows : {_nLeft}, Number of Right Windows : {_nRight}, Number of Rest Windows : {_nRest}.");
		Debug.Log(res);

		return res;
	}

	#endregion
}
