﻿using UnityEngine;

/// <summary> Persistent class to keep settings between scenes. </summary>
public class SettingsManager : MonoBehaviour
{
	private static         SettingsManager _instance = null;
	public static readonly Settings        Settings  = new Settings();

	private void Awake()
	{
		if (_instance != null) { Destroy(gameObject); }
		else {
			_instance = this;
			DontDestroyOnLoad(gameObject);
			Settings.Init();
		}
	}
}
