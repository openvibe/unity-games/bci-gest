﻿///-------------------------------------------------------------------------------------------------
///
/// \file BuildHooks.cs
/// \brief Class For command line Build
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 13/01/2022.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;
using System.IO;
using Builds = System.Collections.Generic.Dictionary<UnityEditor.BuildTarget, Editor.BuildConfig>;

namespace Editor
{
/// <summary> Build and/or run for different <see cref="BuildTarget"/>. Supported platforms are <see cref="BuildTarget.StandaloneWindows64"/>. </summary>
public static class BuildHooks
{
	/// <summary> Path to the builds, relative to the project root folder. </summary>
	private const string BUILD_PATH = "Builds/";

	/// <summary> Path to the resources, relative to the project root folder. </summary>
	private const string RES_DIR = "Resources/";

	/// <summary> Name of the build. </summary>
	private const string FILENAME = "Hand Grasping";

	/// <summary> The path of the scene to build, relative to the project root folder. </summary>
	private static readonly string[] ScenesPath = { "Assets/Scenes/Start Menu.unity", "Assets/Scenes/Game.unity" };

	/// <summary> List of <see cref="BuildConfig"/> for supported <see cref="BuildTarget"/>. </summary>
	private static readonly Builds Configs = new Builds
											 {
												 {
													 BuildTarget.StandaloneWindows64,
													 new BuildConfig
													 {
														 DirectoryPath    = BUILD_PATH + "Windows/",
														 RelativeFilePath = FILENAME + ".exe",
														 Options = new BuildPlayerOptions { scenes = ScenesPath, target = BuildTarget.StandaloneWindows64 },
														 AfterBuild = buildReport => { }
													 }
												 }
											 };

	//----------------------------------------------------------------------------------------------------
	/// <summary> Calls <see cref="Build(BuildTarget, bool)"/> to build for <see cref="BuildTarget.StandaloneWindows64"/>. </summary>
	[MenuItem("Hand Grasping/Build for Windows", priority = 100)]
	public static void BuildWindows() { Build( /*BuildTarget.StandaloneWindows64, andRun: false*/); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Calls <see cref="Build(BuildTarget, bool)"/> to build and run for <see cref="BuildTarget.StandaloneWindows64"/>. </summary>
	[MenuItem("Hand Grasping/Build and Run for Windows", priority = 101)]
	public static void BuildRunWindows() { Build(BuildTarget.StandaloneWindows64, true); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Build for the specified <see cref="BuildTarget"/>. </summary>
	/// <param name="target"> The <see cref="BuildTarget"/> to build. </param>
	/// <param name="andRun"> Automatically run or not the build. </param>
	private static void Build(BuildTarget target = BuildTarget.StandaloneWindows64, bool andRun = false)
	{
		// Get configuration
		if (!Configs.TryGetValue(target, out var config)) { throw new System.ArgumentException($"The {target} platform is not supported.", nameof(target)); }

		// Clear previous build
		var path = new DirectoryInfo(config.DirectoryPath);
		if (path.Exists) { path.Delete(true); }

		// Update build options
		var options = config.Options;
		options.options |= BuildOptions.StrictMode;

		if (andRun) { options.options |= BuildOptions.AutoRunPlayer; }

		config.Options = options;

		// Build
		var report = BuildPipeline.BuildPlayer(config.Options);

		var summary = new System.Text.StringBuilder();
		summary.AppendLine($"Build {target} target {report.summary.result}");
		foreach (var step in report.steps) {
			summary.AppendLine();
			summary.AppendLine(step.ToString());

			foreach (var message in step.messages) { summary.AppendLine($"{message.type}: {message.content}"); }
		}
		Debug.Log(summary);

		// After build
		config.AfterBuild?.Invoke(report);
	}
}
}
