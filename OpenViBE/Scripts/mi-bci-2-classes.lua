function initialize(box)

	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")

	n_trials		= box:get_setting(2) -- 10 trials per class
	first_class		= _G[box:get_setting(3)] -- OVTK_GDF_Left
	second_class 	= _G[box:get_setting(4)] -- OVTK_GDF_Right
	wait_in_out 	= 5
	wait_little 	= 0.5
	wait_beep 		= 1
	display_cue 	= 1.25
	feedback 		= 5
	rest 			= 2.5
	trial_break_min = 3.5
	trial_break_max = 4.5

	-- initializes random seed
	math.randomseed(os.time())

	-- fill the sequence table with predifined order
	sequence = {}
	for i = 1, n_trials do
		table.insert(sequence, 1, first_class)
		table.insert(sequence, 1, second_class)
	end

	-- randomize the sequence
	for i = 1, n_trials do
		a = math.random(1, n_trials*2)
		b = math.random(1, n_trials*2)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end
end

function process(box)
	local t = 0

	-- manages baseline
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + wait_in_out

	-- manages trials
	for i = 1, n_trials * 2 do
		-- first display cross on screen
		box:send_stimulation(1, OVTK_GDF_Start_Of_Trial, t, 0)
		t = t + wait_beep
		-- warn the user the cue is going to appear
		box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
		t = t + wait_little
		-- display cue
		box:send_stimulation(1, sequence[i], t, 0)
		t = t + display_cue
		-- feedback
		box:send_stimulation(1, OVTK_GDF_Feedback_Continuous, t, 0)
		t = t + feedback
		-- Rest
		box:send_stimulation(1, OVTK_StimulationId_RestStart, t, 0)
		t = t + rest
		box:send_stimulation(1, OVTK_StimulationId_RestStop, t, 0)
		t = t + wait_little
		-- ends trial
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
		t = t + math.random(trial_break_min, trial_break_max)
	end

	-- send end for completeness	
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + wait_in_out

	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)
	t = t + wait_little
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)
	
end
