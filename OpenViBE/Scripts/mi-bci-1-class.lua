function initialize(box)

	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")

	n_trials		= box:get_setting(2) 		-- 16
	class_stim		= _G[box:get_setting(3)]	-- Class Stimulations
	wait_in_out 	= 5
	wait_little 	= 0.5
	wait_beep 		= 1
	display_cue 	= 1.25
	feedback 		= 5
	rest 			= 2.5
	trial_break_min = 3.5
	trial_break_max = 4.5

	-- initializes random seed
	math.randomseed(os.time())
end

function process(box)
	local t = 0

	-- manages baseline
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + wait_in_out

	-- manages trials
	for i = 1, n_trials do
		-- first display cross on screen
		box:send_stimulation(1, OVTK_GDF_Start_Of_Trial, t, 0)
		t = t + wait_beep
		-- warn the user the cue is going to appear
		box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
		t = t + wait_little
		-- display cue
		box:send_stimulation(1, class_stim, t, 0)
		t = t + display_cue
		-- feedback
		box:send_stimulation(1, OVTK_GDF_Feedback_Continuous, t, 0)
		t = t + feedback
		-- Rest
		box:send_stimulation(1, OVTK_StimulationId_RestStart, t, 0)
		t = t + rest
		box:send_stimulation(1, OVTK_StimulationId_RestStop, t, 0)
		t = t + wait_little
		-- ends trial
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
		t = t + math.random(trial_break_min, trial_break_max)
	end

	-- send end for completeness	
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + wait_in_out

	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)
	t = t + wait_little
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)
	
end
